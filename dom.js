fetch("https://restcountries.com/v3.1/all")
    .then((dataResponse) => {
        console.log("Data is fetched");
        return dataResponse.json();
    })
    .then((countries) => {
        let loader = document.querySelector('.loader');
        loader.style.display = "none";
        addToHTML(countries);
    })
    .catch((err) => {
        let loader = document.querySelector('.loader');
        loader.style.display = "none";
        displayError();
        console.error(err);
    })

function addToHTML(countries) {
    let section = document.getElementById("countries");
    countries.forEach(country => {
        let countryDiv = document.createElement("div")
        countryDiv.className = "country";
        countryDiv.id = "country";

        let flagDiv = document.createElement("div")
        flagDiv.className = "class-flag";
        flagDiv.id = "class-flag"
        let flag = document.createElement("img");
        flag.src = country.flags.png;
        flag.addEventListener('click', popup);
        flagDiv.appendChild(flag);
        countryDiv.appendChild(flagDiv);

        let info = document.createElement("ul")
        info.className = "class-info";
        info.id = "id-info";

        let name = document.createElement("li");
        name.className = "country-name";
        name.appendChild(document.createTextNode(country.name.official));
        info.appendChild(name);

        let population = document.createElement("li");
        population.className = "country-population";
        population.appendChild(document.createTextNode('Population: ' + country.population));
        info.appendChild(population);

        let region = document.createElement("li");
        region.className = "country-region";
        region.appendChild(document.createTextNode('Region: ' + country.region));
        info.appendChild(region);

        let capital = document.createElement("li");
        capital.className = "country-capital";
        capital.appendChild(document.createTextNode('Capital: ' + country.capital));
        info.appendChild(capital);


        countryDiv.appendChild(info);

        section.appendChild(countryDiv);


        // POPUP
        let infoSection = document.createElement("section");
        infoSection.id = "info-section";
        let infoHeader = document.createElement("div");
        infoHeader.id = "info-header";
        let infoClose = document.createElement("button");
        infoClose.id = "info-close";
        infoClose.appendChild(document.createTextNode("X"));
        infoHeader.appendChild(infoClose);
        infoSection.appendChild(infoHeader);

        let infoMain = document.createElement("div");
        infoMain.id = "info-main";

        let infoFlag = document.createElement("img");
        infoFlag.className = "info-flag";
        infoFlag.id = "info-flag";
        infoFlag.src = country.flags.png;
        infoMain.appendChild(infoFlag);
        let infoDiv = document.createElement("div");
        infoDiv.className = "info-div";
        infoDiv.id = "info-div";

        let infolist = document.createElement("ul")
        infolist.className = "class-info";
        infolist.id = "id-info";

        let infoname = document.createElement("li");
        infoname.className = "info-name";
        infoname.appendChild(document.createTextNode(country.name.official));
        infolist.appendChild(infoname);

        let infopopulation = document.createElement("li");
        infopopulation.className = "info-population";
        infopopulation.appendChild(document.createTextNode('Population: ' + country.population));
        infolist.appendChild(infopopulation);

        let inforegion = document.createElement("li");
        inforegion.className = "info-region";
        inforegion.appendChild(document.createTextNode('Region: ' + country.region));
        infolist.appendChild(inforegion);

        let infocapital = document.createElement("li");
        infocapital.className = "info-capital";
        infocapital.appendChild(document.createTextNode('Capital: ' + country.capital));
        infolist.appendChild(infocapital);

        let nativeName = document.createElement("li");
        nativeName.id = "native-name";
        let native = country.name.nativeName;
        let nativeArray;
        if (native != undefined) {
            nativeArray = Object.entries(country.name.nativeName);
            nativeArray = nativeArray[0][1].official
        }
        else {
            nativeArray = "none";
        }

        nativeName.appendChild(document.createTextNode('Native Name: ' + nativeArray));
        infolist.appendChild(nativeName);

        let subRegion = document.createElement("li");
        subRegion.id = "sub-region";
        subRegion.appendChild(document.createTextNode('Sub Region: ' + country.subregion));
        infolist.appendChild(subRegion);

        let currencies = document.createElement("li");
        currencies.id = "currencies";
        let currency = country.currencies;
        let currencyArray;
        if (currency != undefined) {
            currencyArray = Object.entries(currency);
            currencyArray = currencyArray[0][1].name;
        }
        else {
            currencyArray = "none";
        }
        currencies.appendChild(document.createTextNode('Currencies: ' + currencyArray));
        infolist.appendChild(currencies);

        let languages = document.createElement("li");
        languages.id = "language";
        let languagesArray;
        if (country.languages != undefined) {
            languagesArray = Object.values(country.languages);
        } else {
            languagesArray = "none";
        }
        languages.appendChild(document.createTextNode('Languages: ' + languagesArray));
        infolist.appendChild(languages);

        infoDiv.appendChild(infolist);

        infoMain.appendChild(infoDiv);

        infoSection.appendChild(infoMain);

        let overlay = document.createElement("div")
        overlay.id = "overlay";
        flagDiv.appendChild(overlay);
        flagDiv.appendChild(infoSection);
    });
}
let filter = document.getElementById("region");
filter.addEventListener('click', filterItems);
function displayError() {
    let section = document.getElementById("countries");
    let errorDiv = document.createElement("div");
    errorDiv.className = "error-message";
    errorDiv.innerHTML = "Oops!! Failed while fetching data";
    section.appendChild(errorDiv);
}

function filterItems(e) {
    let option = e.target.value.toLowerCase();
    let countries = document.getElementsByClassName("country");
    Array.from(countries).forEach(country => {
        let countryChilds = country.childNodes[1].childNodes[2];
        let regionName = countryChilds.textContent.toLocaleLowerCase().split(" ");
        if (option == 'all' || option == "choose") {
            country.style.display = "block";
        }
        else {
            if (regionName[1] == option) {
                console.log(regionName[1]);
                country.style.display = "block";
            } else {
                country.style.display = "none";
            }
        }
    })

}

function popup(e) {

    let overlay = e.target.nextElementSibling;
    let popup = overlay.nextElementSibling;
    let closeBtn = popup.firstChild.firstChild;
    overlay.classList.add("overlay");
    popup.classList.add("show");

    document.addEventListener("keydown", function (e) {
        if (e.key == "Escape") {
            closePopup(popup, overlay);
        }
    })
    overlay.addEventListener("click", function (e) {
        closePopup(popup, overlay);
    })
    closeBtn.addEventListener('click', () => {
        closePopup(popup, overlay);
    })
}

function closePopup(popup, overlay) {
    popup.classList.remove("show");
    overlay.classList.remove("overlay");
}
